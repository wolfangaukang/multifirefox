{
  description = "Equivalent of `firefox -P PROFILE_NAME -no-remote` but with the remote enabled";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";

  outputs = { self, nixpkgs, ... }:
    let
      systemPkgs = nixpkgs.legacyPackages;
      systems = nixpkgs.lib.systems.flakeExposed;
      forEachSystem = nixpkgs.lib.genAttrs systems;

    in {
      packages = forEachSystem (system: {
        multifirefox = (systemPkgs.${system}).callPackage ./package.nix {};
        default = self.outputs.packages.${system}.multifirefox;
      });
      apps = forEachSystem (system: {
        multifirefox = { type = "app"; program = "${nixpkgs.lib.getExe self.outputs.packages.${system}.default}"; };
        default = self.outputs.apps.${system}.multifirefox;
      });
      devShells = forEachSystem (system:
        let
          pkgs = systemPkgs.${system};
          inherit (pkgs) mkShell zenity;
        in {
          default = mkShell { buildInputs = [ zenity ]; };
        });
      overlays.default = final: prev: { inherit (self.packages.${final.system}) multifirefox; };
    };
}
