{ lib
, resholve
, fetchurl
, fetchgit
, bash
, firefox
, zenity
}:

let
  desktopFile = fetchurl {
    url = "https://gist.githubusercontent.com/mildred/c44d6ca8ac76333ca4a2/raw/f71167ad2e35a2bb2166047e5ecfd7c0a5ea17ad/multifirefox.desktop";
    sha256 = "sha256-m/BjqfgQ0RCdCRSmIly3jzpwlQ3HaHu+mejGScWo/vY=";
  };

in resholve.mkDerivation {
  pname = "multifirefox";
  version = "unstable-2024-03-18";
  src = fetchgit {
    url = "https://gist.github.com/mildred/c44d6ca8ac76333ca4a2";
    rev = "f71167ad2e35a2bb2166047e5ecfd7c0a5ea17ad";
    hash = "sha256-Bix4S/MUW5MzAA0/xyO63EpFAzLGRtwJ3sJYwl4ahT4=";
  };

  dontBuild = true;
  dontConfigure = true;

  patches = [
    ./zenity.patch
  ];

  installPhase = ''
    runHook preInstall

    mkdir -p $out/bin $out/share/applications
    install -Dm755 multifirefox $out/bin/multifirefox
    install -Dm644 ${desktopFile} $out/share/applications/multifirefox.desktop
    cp -R ${firefox}/share/icons $out/share/

    runHook postInstall
  '';

  solutions = {
    default = {
      scripts = [ "bin/multifirefox" ];
      interpreter = "${lib.getExe bash}";
      inputs = [ firefox zenity ];
      execer = [
        "cannot:${zenity}/bin/zenity"
      ];
      fake = {
        external = [ "cut" "firefox" "firefox-developer" "grep" ];
      };
    };
  };

  meta = with lib; {
    description = "Equivalent of `firefox -P PROFILE_NAME -no-remote` but with the remote enabled";
    homepage = "https://codeberg.org/wolfangaukang/multifirefox";
    license = licenses.gpl3Only;
    maintainers = with maintainers; [ wolfangaukang ];
    mainProgram = "multifirefox";
    platforms = platforms.linux;
  };
}
