# multifirefox

## NOTE: All Credits to [mildred](https://gist.github.com/mildred), who created this script. Check it [here](https://gist.github.com/mildred/c44d6ca8ac76333ca4a2). I'm just making some modifications.

Equivalent of `firefox -P PROFILE_NAME -no-remote` but with the remote enabled.

Multifirefox is a way to have multiple firefox profiles and open links in them. There are multiple ways of invocation:

    multifirefox PROFILE_NAME ...

Start Firefox with the profile PROFILE_NAME (or open a new window in the existing firefox instance).

    multifirefox [-] ...

Ask for the profile then start firefox (or open a window in the running Firefox profile).

Installation
------------

- put `multifirefox` on your PATH

- put `multifirefox.desktop` in `~/.local/share/applications`

- run:

        xdg-mime default multifirefox.desktop x-scheme-handler/http x-scheme-handler/https

- on GNOME, run:

        gvfs-mime --set x-scheme-handler/http multifirefox.desktop
        gvfs-mime --set x-scheme-handler/https multifirefox.desktop
