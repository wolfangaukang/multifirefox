# TODO

- [ ] Implement set to control application flow
  - [ ] Fix the code accordingly
- [ ] See why zenity is not throwing any messages
- [ ] Find why Firefox throws the grey bar instead of integrating the exit button on the tab bar
